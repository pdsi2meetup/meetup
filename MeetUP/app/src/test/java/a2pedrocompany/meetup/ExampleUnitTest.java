package a2pedrocompany.meetup;

import org.junit.Test;

import java.util.List;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.Dao.Events;
import a2pedrocompany.meetup.Dao.Usuario;
import a2pedrocompany.meetup.Dao.UsuarioEvento;
import a2pedrocompany.meetup.Dao.UsuarioEventos;
import a2pedrocompany.meetup.Dao.Usuarios;
import retrofit2.Response;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest
{
    @Test
    public void getEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Response<Evento> call = servH.getEvent(3).execute();
        Evento evento = call.body();
    }

    @Test
    public void deleteEvent() throws Exception {
    HttpClient dbH = new HttpClient();

    ServiceInterface servH = dbH.service;

    Response<Integer> call = servH.deleteEvent(12).execute();
    Integer evento = call.body();
}

    @Test
    public void updateEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Evento eventoSend = new Evento(3, "rede", 1, 1);

        Response<Integer> call = servH.updateEvent(3, eventoSend).execute();
    }

    @Test
    public void createEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Evento eventoSend = new Evento("teste_add_api", 1, 2);

        Response<Integer> call = servH.addEvent(eventoSend).execute();

        int id = call.body();

        System.out.print(id);
    }

    @Test
    public void getAllEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Response<Events> call = servH.getEvents().execute();
        Events eventos = call.body();

        List<Evento> eventosFinal = eventos.ToEventList();
        eventosFinal.isEmpty();
    }

    @Test
    public void doLoginTest() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Response<Usuarios> call = servH.doLogin("email,eq,pedro@meetup.tk").execute();
        Usuarios users = call.body();

        List<Usuario> eventosFinal = users.toUserList();
        eventosFinal.isEmpty();
    }

    @Test
    public void createUserEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        UsuarioEvento participa = new UsuarioEvento(27, 1);

        Response<Integer> call = servH.addParticipaEvento(participa).execute();

        int id = call.body();

        System.out.print(id);
    }

    @Test
    public void getAllUserEvent() throws Exception {
        HttpClient dbH = new HttpClient();

        ServiceInterface servH = dbH.service;

        Response<UsuarioEventos> call = servH.getUserEvents().execute();
        UsuarioEventos eventos = call.body();

        List<UsuarioEvento> eventosFinal = eventos.ToEventList();
        eventosFinal.isEmpty();
    }
}