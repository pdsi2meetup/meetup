package a2pedrocompany.meetup.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.Dao.Events;
import a2pedrocompany.meetup.Dao.UsuarioEvento;
import a2pedrocompany.meetup.Dao.UsuarioEventos;
import a2pedrocompany.meetup.HttpClient;
import a2pedrocompany.meetup.ListViewHelper;
import a2pedrocompany.meetup.R;
import a2pedrocompany.meetup.ServiceInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * Created by Pedro on 11/24/2016.
 */

public class ListEventActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    ServiceInterface servH = new HttpClient().service;

    private ListView lvListagem;
    private ListView nearbyListagem;
    private ListView otherListagem;
    private ListView joinedListagem;

    private List<Evento> eventList;
    private List<UsuarioEvento> userEventList;
    private List<Evento> myEventList = new ArrayList();
    private List<Evento> nearbyEventList = new ArrayList();
    private List<Evento> otherEventList = new ArrayList();
    private List<Evento> joinedEventList = new ArrayList();

    private Button addEventBtn;
    private MyAdapter adapter;
    private NearbyAdapter nAdapter;
    private OtherAdapter oAdapter;
    private JoinedAdapter jAdapter;

    private Evento selectedEvent = null;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;

    int user_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_event);

        Intent intent = getIntent();
        user_id = intent.getIntExtra("USER_ID", 0);

        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }


        addEventBtn = (Button) findViewById(R.id.addEvent);
        addEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ListEventActivity.this, CreateEvent.class);
                myIntent.putExtra("USER_ID", user_id);
                ListEventActivity.this.startActivity(myIntent);
            }
        });

        this.adapter = new MyAdapter();
        this.nAdapter = new NearbyAdapter();
        this.oAdapter = new OtherAdapter();
        this.jAdapter = new JoinedAdapter();

        lvListagem = (ListView) findViewById(R.id.myListagem);

        lvListagem.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedEvent = (Evento) lvListagem.getItemAtPosition(i);
                return false;
            }
        });

        lvListagem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent form = new Intent(ListEventActivity.this, ViewEventActivity.class);
                selectedEvent = (Evento) lvListagem.getItemAtPosition(i);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                form.putExtra("FLAG_BTN", 1);
                startActivity(form);
            }
        });

        registerForContextMenu(lvListagem);

        nearbyListagem = (ListView) findViewById(R.id.nearbyListagem);

        nearbyListagem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent form = new Intent(ListEventActivity.this, ViewEventActivity.class);
                selectedEvent = (Evento) nearbyListagem.getItemAtPosition(i);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                form.putExtra("USER_ID", user_id);
                startActivity(form);
            }
        });

        joinedListagem = (ListView) findViewById(R.id.joinedListagem);

        joinedListagem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent form = new Intent(ListEventActivity.this, ViewEventActivity.class);
                selectedEvent = (Evento) joinedListagem.getItemAtPosition(i);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                form.putExtra("USER_ID", user_id);
                form.putExtra("FLAG_BTN", 1);
                startActivity(form);
            }
        });

        otherListagem = (ListView) findViewById(R.id.otherListagem);

        otherListagem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent form = new Intent(ListEventActivity.this, ViewEventActivity.class);
                selectedEvent = (Evento) otherListagem.getItemAtPosition(i);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                form.putExtra("USER_ID", user_id);
                startActivity(form);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        getMenuInflater().inflate(R.menu.list_event_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent form;
        switch (item.getItemId()) {
            case R.id.deleteMenu:
                new ExcluirEvento().execute();
                break;
            case R.id.editMenu:
                form = new Intent(ListEventActivity.this, EditEventActivity.class);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                form.putExtra("USER_ID", user_id);
                startActivity(form);
                break;
            case R.id.paidMenu:
                form = new Intent(ListEventActivity.this, PaidEventActivity.class);
                form.putExtra("EVENT_ID", selectedEvent.getId());
                startActivity(form);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    synchronized protected void onResume() {
        super.onResume();


        Call<UsuarioEventos> callUserEvent = servH.getJoinedUserEvents("usuario_id,eq," + user_id);
        callUserEvent.enqueue
                (
                        new Callback<UsuarioEventos>() {
                            @Override
                            public void onResponse(Call<UsuarioEventos> call, Response<UsuarioEventos> response) {
                                Object lock = new Object();
                                try {
                                    userEventList = response.body().ToEventList();
//                                    synchronized (lock) {
//                                        lock.notifyAll();
//                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<UsuarioEventos> call, Throwable t) {
                                t.printStackTrace();
                            }
                        }
                );

        myEventList = new ArrayList<>();
        otherEventList = new ArrayList<>();
        nearbyEventList = new ArrayList<>();
        joinedEventList = new ArrayList<>();

        Call<Events> call = servH.getEvents();
        call.enqueue
                (
                        new Callback<Events>() {
                            @Override
                            public void onResponse(Call<Events> call, Response<Events> response) {
                                Object lock = new Object();
                                try {
                                    eventList = response.body().ToEventList();
//                                    if (userEventList == null)
//                                        synchronized (lock) {
//                                            lock.wait();
//                                        }
                                    for (Evento e : eventList) {
                                        if (e.getId_usuario() == user_id) {
                                            myEventList.add(e);
                                            //eventList.remove(e);
                                        } else {
                                            int s = joinedEventList.size();

                                            for (UsuarioEvento ue : userEventList) {
                                                if (ue.getId_evento() == e.getId()) {
                                                    joinedEventList.add(e);
                                                    break;
                                                }
                                            }
                                            if (joinedEventList.size() == s) {
                                                double d = -1;
                                                if (mLastLocation == null) {
                                                    Toast.makeText(ListEventActivity.this, "Habilite sua localizaçao", Toast.LENGTH_LONG).show();
                                                }
                                                else
                                                {
                                                    double dlon = mLastLocation.getLongitude() - e.getLongitude();
                                                    double dlat = mLastLocation.getLatitude() - e.getLatitude();
                                                    double a = (sin(dlat / 2) * sin(dlat / 2)) + cos(mLastLocation.getLatitude()) * cos(e.getLatitude()) * ((sin(dlon / 2)) * (sin(dlon / 2)));
                                                    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
                                                    d = 6373 * c;
                                                }


                                                if (d <= 0.5 && d >= 0)
                                                    nearbyEventList.add(e);
                                                else
                                                    otherEventList.add(e);
                                            }
                                        }
                                    }


                                    lvListagem.setAdapter(adapter);
                                    nearbyListagem.setAdapter(nAdapter);
                                    otherListagem.setAdapter(oAdapter);
                                    joinedListagem.setAdapter(jAdapter);
                                    ListViewHelper.setListViewHeightBasedOnChildren(lvListagem);
                                    ListViewHelper.setListViewHeightBasedOnChildren(nearbyListagem);
                                    ListViewHelper.setListViewHeightBasedOnChildren(otherListagem);
                                    ListViewHelper.setListViewHeightBasedOnChildren(joinedListagem);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<Events> call, Throwable t) {
                                t.printStackTrace();
                            }
                        }
                );
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mGoogleApiClient, getIndexApiAction());
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("CreateEvent Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    class MyAdapter extends BaseAdapter
    {
        LayoutInflater mInflater;
        TextView tv1, tv;

        MyAdapter()
        {
            mInflater = (LayoutInflater) ListEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return myEventList.size();
        }

        @Override
        public Object getItem(int position) {
            return myEventList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View vi = convertView;


            if (convertView == null)
                vi = mInflater.inflate(R.layout.row, null);

            tv = (TextView) vi.findViewById(R.id.eventName);
            tv1 = (TextView) vi.findViewById(R.id.eventDesc);

            tv.setText(myEventList.get(position).getNome());
            tv1.setText("Descricao: " + myEventList.get(position).getDesc());

            return vi;
        }
    }

    class NearbyAdapter extends BaseAdapter
    {
        LayoutInflater mInflater;
        TextView tv1, tv;

        NearbyAdapter()
        {
            mInflater = (LayoutInflater) ListEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return nearbyEventList.size();
        }

        @Override
        public Object getItem(int position) {
            return nearbyEventList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View vi = convertView;


            if (convertView == null)
                vi = mInflater.inflate(R.layout.row, null);

            tv = (TextView) vi.findViewById(R.id.eventName);
            tv1 = (TextView) vi.findViewById(R.id.eventDesc);

            tv.setText(nearbyEventList.get(position).getNome());
            tv1.setText("Descricao: " + nearbyEventList.get(position).getDesc());

            return vi;
        }
    }

    class OtherAdapter extends BaseAdapter
    {
        LayoutInflater mInflater;
        TextView tv1, tv;

        OtherAdapter()
        {
            mInflater = (LayoutInflater) ListEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return otherEventList.size();
        }

        @Override
        public Object getItem(int position) {
            return otherEventList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View vi = convertView;


            if (convertView == null)
                vi = mInflater.inflate(R.layout.row, null);

            tv = (TextView) vi.findViewById(R.id.eventName);
            tv1 = (TextView) vi.findViewById(R.id.eventDesc);

            tv.setText(otherEventList.get(position).getNome());
            tv1.setText("Descricao: " + otherEventList.get(position).getDesc());

            return vi;
        }
    }

    class JoinedAdapter extends BaseAdapter
    {
        LayoutInflater mInflater;
        TextView tv1, tv;

        JoinedAdapter()
        {
            mInflater = (LayoutInflater) ListEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return joinedEventList.size();
        }

        @Override
        public Object getItem(int position) {
            return joinedEventList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View vi = convertView;


            if (convertView == null)
                vi = mInflater.inflate(R.layout.row, null);

            tv = (TextView) vi.findViewById(R.id.eventName);
            tv1 = (TextView) vi.findViewById(R.id.eventDesc);

            tv.setText(joinedEventList.get(position).getNome());
            tv1.setText("Descricao: " + joinedEventList.get(position).getDesc());

            return vi;
        }
    }

    private class ExcluirEvento extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ListEventActivity.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Excluindo Evento");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
            Toast.makeText(ListEventActivity.this, result[0], Toast.LENGTH_LONG).show();
            finish();
            startActivity(getIntent());
        }

        @Override
        protected String[] doInBackground(String... params)
        {
            try
            {
                Response<Integer> call = servH.deleteEvent(selectedEvent.getId()).execute();

                if(call.code() == 200 && call.body() == 1){

                    return new String[]{"Deletado com sucesso!"};
                } else {
                    return new String[]{"Erro ao enviar!"};
                }

            }
            catch (Exception e)
            {
                return new String[]{"erro transmissão: " + e.getMessage()};
            }
        }

    }
}
