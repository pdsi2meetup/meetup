package a2pedrocompany.meetup.Activities;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.Dao.Usuario;
import a2pedrocompany.meetup.Dao.Usuarios;
import a2pedrocompany.meetup.HttpClient;
import a2pedrocompany.meetup.R;
import a2pedrocompany.meetup.ServiceInterface;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button botao;
    private EditText email;
    private EditText senha;

    Usuario loggedUser;

    String login = "";
    String password = "";
    ServiceInterface servH = new HttpClient().service;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botao = (Button) findViewById(R.id.loginBtn);
        email = (EditText) findViewById(R.id.emailTxt);
        senha = (EditText) findViewById(R.id.passwordTxt);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login = email.getText().toString();
                password = senha.getText().toString();
                new DoLogin().execute();
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class DoLogin extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Transmitindo Dados");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
            Toast.makeText(MainActivity.this, result[0], Toast.LENGTH_LONG).show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            try {

                Response<Usuarios> call = servH.doLogin("email,eq,"+login).execute();
                boolean success = false;

                if (call.body() != null) {
                    List<Usuario> userList = call.body().toUserList();

                    for(Usuario u : userList)
                    {
                        if(u.getSenha().equals(password) && u.getEmail().equals(login))
                        {
                            Intent form = new Intent(MainActivity.this, ListEventActivity.class);
                            form.putExtra("USER_ID", u.getId());
                            startActivity(form);
                            return new String[]{"Login bem sucedido"};
                        }
                    }

                    return new String[]{"Senha inválida"};
                } else {
                    return new String[]{"Erro ao enviar!"};
                }

            } catch (Exception e) {
                return new String[]{"Exceção: " + e.getMessage()};
            }

        }
    }
}
