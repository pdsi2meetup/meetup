package a2pedrocompany.meetup.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.MapHelper;
import a2pedrocompany.meetup.HttpClient;
import a2pedrocompany.meetup.R;
import a2pedrocompany.meetup.ServiceInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pedro on 12/1/2016.
 */

public class PaidEventActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Button editButton;
    private TextView nome;
    private TextView preco;
    private TextView alcance;
    private Button increaseReach;
    private Button decreaseReach;

    int event_id;
    String ret = "";
    ServiceInterface servH = new HttpClient().service;
    private Evento evento;
    Evento editEvento;

    GoogleMap gMapa = null;
    List<Address> addresses = new ArrayList<Address>();
    Geocoder geocoder;
    CircleOptions circle;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paid_event);

        Intent intent = getIntent();
        event_id = intent.getIntExtra("EVENT_ID", 0);

        editButton = (Button) findViewById(R.id.editButton);
        nome = (TextView) findViewById(R.id.eventName);
        preco = (TextView) findViewById(R.id.priceText);
        alcance = (TextView) findViewById(R.id.reachTxt);
        increaseReach = (Button) findViewById(R.id.increaseRange);
        decreaseReach = (Button) findViewById(R.id.decreaseRange);

        AsyncTask<String, Void, String[]> variable = new TransmitirTask().execute();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Integer> call = servH.updateEvent(event_id, evento);
                call.enqueue
                        (
                                new Callback<Integer>() {
                                    @Override
                                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                                        int result = response.body();
                                    }

                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t) {
                                        t.printStackTrace();
                                    }
                                }
                        );
            }
        });

        increaseReach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer value = (Integer.parseInt((String) alcance.getText()) + 1);
                String text = value.toString();
                alcance.setText(text);
                circle.radius(Float.parseFloat(alcance.getText().toString())*1000);
                gMapa.clear();
                gMapa.addCircle(circle);
            }
        });

        decreaseReach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer value = (Integer.parseInt((String) alcance.getText()) - 1) >= 2 ? (Integer.parseInt((String) alcance.getText()) - 1) : 2;
                String text = value.toString();
                alcance.setText(text);
                circle.radius(Float.parseFloat(alcance.getText().toString())*1000);
                gMapa.clear();
                gMapa.addCircle(circle);
            }
        });

        editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                editEvento = new Evento();
                editEvento.setId(event_id);
                editEvento.setValor(1);
                editEvento.setAlcance(alcance.getText().toString());
                editEvento.setId_categoria_evento(evento.getId_categoria_evento());
                editEvento.setLatitude(evento.getLatitude());
                editEvento.setLongitude(evento.getLongitude());
                editEvento.setId_usuario(evento.getId_usuario());

                Call<Integer> call = servH.updateEvent(event_id, editEvento);
                call.enqueue
                        (
                                new Callback<Integer>()
                                {
                                    @Override
                                    public void onResponse(Call<Integer> call, Response<Integer> response)
                                    {
                                        int result = response.body();
                                        if (response.code() == 200) {

                                            Toast.makeText(PaidEventActivity.this, "Boleto gerado", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(PaidEventActivity.this, "Erro ao gerar boleto", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t)
                                    {
                                        t.printStackTrace();
                                    }
                                }
                        );
            }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMapa = map;
        //gMapa.addCircle(circle);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("PaidEvent Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class TransmitirTask extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(PaidEventActivity.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Transmitindo Dados");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
            nome.setText(evento.getNome());
            alcance.setText(evento.getAlcance());
            preco.setText(String.valueOf(evento.getValor()));

            try {
                addresses = geocoder.getFromLocationName(evento.getLocal(), 5);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses.size() > 0) {
                circle = new CircleOptions().center(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                        .radius(Float.parseFloat(alcance.getText().toString())*1000);
                MapHelper.moveTo(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()), gMapa);
                increaseReach.performClick();
                decreaseReach.performClick();
            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(PaidEventActivity.this);
                adb.setTitle("Google Map");
                adb.setMessage("Forneça uma localização válida");
                adb.setPositiveButton("Close", null);
                adb.show();
            }
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                Response<Evento> call = servH.getEvent(event_id).execute();
                evento = call.body();
                ret = new String("Enviado com sucesso!");
            } catch (Exception e) {
                return new String[]{"erro transmissão: " + e.getMessage()};
            }
            return new String[]{ret};
        }
    }
}
