package a2pedrocompany.meetup.Activities;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.MapHelper;
import a2pedrocompany.meetup.HttpClient;
import a2pedrocompany.meetup.R;
import a2pedrocompany.meetup.ServiceInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pedro on 11/24/2016.
 */

public class EditEventActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Button editButton;
    private EditText nome;
    private EditText hora_inicio;
    private EditText hora_final;
    private EditText endereco;
    private EditText descricao;

    int event_id;
    int user_id;
    String ret = "";
    ServiceInterface servH = new HttpClient().service;
    private Evento evento;

    Location mLastLocation = new Location("");
    GoogleMap gMapa;
    List<Address> addresses = new ArrayList<Address>();
    Geocoder geocoder;
    MarkerOptions marker;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_event);

        Intent intent = getIntent();
        event_id = intent.getIntExtra("EVENT_ID", 0);
        user_id = intent.getIntExtra("USER_ID", 0);

        editButton = (Button) findViewById(R.id.createEventBtn);
        nome = (EditText) findViewById(R.id.eventName);
        hora_inicio = (EditText) findViewById(R.id.initialTimeTxt);
        hora_final = (EditText) findViewById(R.id.endTimeTxt);
        endereco = (EditText) findViewById(R.id.addressTxt);
        descricao = (EditText) findViewById(R.id.descTxt);

        new TransmitirTask().execute();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evento = new Evento();
                evento.setId(event_id);
                evento.setNome(nome.getText().toString());
                evento.setHorario_inicio(hora_inicio.getText().toString());
                evento.setLocal(endereco.getText().toString());
                evento.setDesc(descricao.getText().toString());
                evento.setId_usuario(user_id);
                evento.setId_categoria_evento(1);
                evento.setLongitude(mLastLocation.getLongitude());
                evento.setLatitude(mLastLocation.getLatitude());
                Call<Integer> call = servH.updateEvent(event_id, evento);
                call.enqueue
                        (
                                new Callback<Integer>() {
                                    @Override
                                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                                        int result = response.body();
                                        if (response.code() == 200) {

                                            Toast.makeText(EditEventActivity.this, "Atualizado com sucesso", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(EditEventActivity.this, "Erro ao atualizar", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t) {
                                        t.printStackTrace();
                                    }
                                }
                        );
            }
        });

        hora_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EditEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora_final.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        hora_inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EditEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora_inicio.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        endereco.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                List<Address> addresses = new ArrayList<Address>();
                try {
                    addresses = geocoder.getFromLocationName(endereco.getText().toString(), 5);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                if (addresses.size() > 0) {
                    gMapa.clear();
                    marker = new MarkerOptions()
                            .position(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                            .title("Marker")
                            .draggable(true);
                    gMapa.addMarker(marker);
                    MapHelper.moveTo(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()), gMapa);
                    mLastLocation.setLatitude(addresses.get(0).getLatitude());
                    mLastLocation.setLongitude(addresses.get(0).getLongitude());
                } else {
                    AlertDialog.Builder adb = new AlertDialog.Builder(EditEventActivity.this);
                    adb.setTitle("Google Map");
                    adb.setMessage("Forneça uma localização válida");
                    adb.setPositiveButton("Close", null);
                    adb.show();
                }
            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMapa = map;

        gMapa.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                mLastLocation.setLatitude(marker.getPosition().latitude);
                mLastLocation.setLongitude(marker.getPosition().longitude);

                try {
                    addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();

                endereco.setText(address + ", " + city + ", " + state);
            }
        });


    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("EditEvent Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class TransmitirTask extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(EditEventActivity.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Transmitindo Dados");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
            nome.setText(evento.getNome());
            endereco.setText(evento.getLocal());
            descricao.setText(evento.getDesc());
            hora_final.setText(evento.getHorario_fim());
            hora_inicio.setText(evento.getHorario_inicio());
            String errorMessage = "";

            try {
                addresses = geocoder.getFromLocationName(endereco.getText().toString(), 5);
            } catch (IOException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            }

            if (addresses.size() > 0) {
                marker = new MarkerOptions()
                        .position(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                        .title("Marker")
                        .draggable(true);
                endereco.requestFocus();
                nome.requestFocus();
            } else {
                errorMessage = errorMessage == "" ? "Forneça uma localização válida" : errorMessage;
                AlertDialog.Builder adb = new AlertDialog.Builder(EditEventActivity.this);
                adb.setTitle("Google Map");
                adb.setMessage(errorMessage);
                adb.setPositiveButton("Close", null);
                adb.show();
            }
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                Response<Evento> call = servH.getEvent(event_id).execute();
                evento = call.body();
            } catch (Exception e) {
                return new String[]{"erro transmissão: " + e.getMessage()};
            }
            return new String[]{ret};
        }
    }
}
