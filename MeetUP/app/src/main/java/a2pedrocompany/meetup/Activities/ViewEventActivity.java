package a2pedrocompany.meetup.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.Dao.UsuarioEvento;
import a2pedrocompany.meetup.HttpClient;
import a2pedrocompany.meetup.MapHelper;
import a2pedrocompany.meetup.R;
import a2pedrocompany.meetup.ServiceInterface;
import retrofit2.Response;

/**
 * Created by Pedro on 11/30/2016.
 */

public class ViewEventActivity extends AppCompatActivity implements OnMapReadyCallback
{

        private TextView nome;
        private TextView hora_inicio;
        private TextView hora_final;
        private TextView endereco;
        private TextView descricao;
        private Button takePartBtn;


        UsuarioEvento participacao = new UsuarioEvento();
        int event_id, user_id;
        String ret = "";
        ServiceInterface servH = new HttpClient().service;
        private Evento evento;

        GoogleMap gMapa;
        List<Address> addresses = new ArrayList<Address>();
        Geocoder geocoder;
        MarkerOptions marker = null;


        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.view_event);

            Intent intent = getIntent();
            event_id = intent.getIntExtra("EVENT_ID", 0);
            user_id = intent.getIntExtra("USER_ID", 0);
            int flag = intent.getIntExtra("FLAG_BTN", 0);

            nome = (TextView) findViewById(R.id.eventName);
            hora_inicio = (TextView) findViewById(R.id.initialTimeTxt);
            hora_final = (TextView) findViewById(R.id.endTimeTxt);
            endereco = (TextView) findViewById(R.id.addressTxt);
            descricao = (TextView) findViewById(R.id.descTxt);
            takePartBtn = (Button) findViewById(R.id.takePartBtn);

            new TransmitirTask().execute();

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
            mapFragment.getMapAsync(this);

            geocoder = new Geocoder(this, Locale.getDefault());

            takePartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    participacao.setId_usuario(user_id);
                    participacao.setId_evento(event_id);
                    new Participar().execute();
                }
            });

            if(flag == 1) {
                takePartBtn.setClickable(false);
                takePartBtn.setEnabled(false);
                takePartBtn.setVisibility(View.INVISIBLE);
            }

            endereco.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        addresses = geocoder.getFromLocationName(endereco.getText().toString(), 5);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (addresses.size() > 0) {
                        marker = new MarkerOptions()
                                .position(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                                .title("Marker")
                                .draggable(true);
                        gMapa.addMarker(marker);
                        MapHelper.moveTo(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()), gMapa);
                    }
                }
            });

        }

        @Override
        public void onMapReady(GoogleMap map)
        {
            gMapa = map;

            if (marker != null)
            {
                gMapa.addMarker(marker);
            }
        }

        private class TransmitirTask extends AsyncTask<String, Void, String[]>{

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = new ProgressDialog(ViewEventActivity.this);
                dialog.setTitle("Aguarde...");
                dialog.setMessage("Transmitindo Dados");
                dialog.setCancelable(false);
                dialog.setIndeterminate(false);
                dialog.show();
            }

            @Override
            protected void onPostExecute(String[] result) {

                dialog.dismiss();
                nome.setText(evento.getNome());
                endereco.setText(evento.getLocal());
                descricao.setText(evento.getDesc());
                hora_final.setText(evento.getHorario_fim());
                hora_inicio.setText(evento.getHorario_inicio());

                try {
                    addresses = geocoder.getFromLocationName(endereco.getText().toString(), 5);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses.size() > 0) {
                    marker = new MarkerOptions()
                            .position(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                            .title("Marker")
                            .draggable(true);
                }
            }

            @Override
            protected String[] doInBackground(String... params) {

                try {
                    Response<Evento> call = servH.getEvent(event_id).execute();
                    evento = call.body();
                    ret = new String("Enviado com sucesso!");
                } catch (Exception e) {
                    return new String[]{"erro transmissão: " + e.getMessage()};
                }
                return new String[]{ret};
            }
        }

    private class Participar extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ViewEventActivity.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Transmitindo Dados");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                Response<Integer> call = servH.addParticipaEvento(participacao).execute();
                //int id = call.body();
                ret = new String("Enviado com sucesso!");
            } catch (Exception e) {
                return new String[]{"erro transmissão: " + e.getMessage()};
            }
            return new String[]{ret};
        }
    }
}
