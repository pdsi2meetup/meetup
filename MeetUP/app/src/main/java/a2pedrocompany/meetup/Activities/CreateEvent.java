package a2pedrocompany.meetup.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import a2pedrocompany.meetup.*;
import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.MapHelper;
import a2pedrocompany.meetup.R;
import retrofit2.Response;

/**
 * Created by Pedro on 10/20/2016.
 */

public class CreateEvent extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{

    private Button botao;
    private EditText nome;
    private EditText hora_inicio;
    private EditText hora_final;
    private EditText descricao;
    private EditText endereco;

    private Evento e;
    int user_id = 0;

    ServiceInterface servH = new HttpClient().service;
    GoogleApiClient mGoogleApiClient;
    GoogleMap gMapa;
    Location mLastLocation;
    List<Address> addresses = new ArrayList<Address>();
    Geocoder geocoder;
    MarkerOptions marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(a2pedrocompany.meetup.R.layout.create_event);

        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }

        Intent intent = getIntent();
        user_id = intent.getIntExtra("USER_ID", 0);

        botao = (Button) findViewById(R.id.createEventBtn);
        nome = (EditText) findViewById(R.id.eventName);
        hora_inicio = (EditText) findViewById(R.id.initialTimeTxt);
        hora_final = (EditText) findViewById(R.id.endTimeTxt);
        endereco = (EditText) findViewById(R.id.addressTxt);
        descricao = (EditText) findViewById(R.id.descTxt);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                e = new Evento();
                e.setNome(nome.getText().toString());
                e.setHorario_inicio(hora_inicio.getText().toString());
                e.setHorario_fim(hora_final.getText().toString());
                e.setStatus("True");
                e.setValor(0);
                e.setAlcance("2");
                e.setLocal(endereco.getText().toString());
                e.setId_categoria_evento(1);
                e.setId_usuario(user_id);
                e.setDesc(descricao.getText().toString());
                e.setLatitude(mLastLocation.getLatitude());
                e.setLongitude(mLastLocation.getLongitude());

                new TransmitirTask().execute();
            }
        });

        hora_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora_final.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        hora_inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora_inicio.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        endereco.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus)
            {
                List<Address> addresses = new ArrayList<Address>();
                String errorMessage = "";
                try {
                    addresses = geocoder.getFromLocationName(endereco.getText().toString(), 5);
                } catch (IOException e1) {
                    e1.printStackTrace();
                    errorMessage = "Geocoder: " + e1.getMessage();
                }

                if(addresses.size() > 0)
                {
                    gMapa.clear();
                    marker =  new MarkerOptions()
                            .position(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()))
                            .title("Marker")
                            .draggable(true);
                    gMapa.addMarker(marker);
                    MapHelper.moveTo(new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude()), gMapa);
                    mLastLocation.setLatitude(addresses.get(0).getLatitude());
                    mLastLocation.setLongitude( addresses.get(0).getLongitude());
                }
                else {
                    errorMessage = errorMessage == "" ? "Forneça uma localização válida" : errorMessage;
                    AlertDialog.Builder adb = new AlertDialog.Builder(CreateEvent.this);
                    adb.setTitle("Google Map");
                    adb.setMessage(errorMessage);
                    adb.setPositiveButton("Close",null);
                    adb.show();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gMapa = map;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
        {
            marker = new MarkerOptions()
                .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                .title("Marker")
                .draggable(true);

            gMapa.addMarker(marker);
            MapHelper.moveTo(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), gMapa);

            try {
                addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();

            endereco.setText(address + ", " +  city + ", " +  state);

            gMapa.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker)
                {
                    mLastLocation.setLatitude(marker.getPosition().latitude);
                    mLastLocation.setLongitude(marker.getPosition().longitude);

                    try {
                        addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();

                    endereco.setText(address + ", " +  city + ", " +  state);
                }
            });

//            gMapa.addCircle(new CircleOptions()
//                    .center(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
//                    .radius(5.0)
//            );
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("CreateEvent Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    private class TransmitirTask extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(CreateEvent.this);
            dialog.setTitle("Aguarde...");
            dialog.setMessage("Transmitindo Dados");
            dialog.setCancelable(false);
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String[] result) {
            dialog.dismiss();
            Toast.makeText(CreateEvent.this, result[0], Toast.LENGTH_LONG).show();
        }

        @Override
        protected String[] doInBackground(String... params)
        {
            try
            {
                Response<Integer> call = servH.addEvent(e).execute();
                //id_evento = call.body();

                if(call.code() == 200){

                    return new String[]{"Envento criado com sucesso com sucesso!"};
                } else {
                    return new String[]{"Erro ao enviar!"};
                }
            }
            catch (Exception e)
            {
                return new String[]{"Excessao: " + e.getMessage()};
            }
        }

    }
}
