package a2pedrocompany.meetup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Pedro on 10/3/2016.
 */

public class DbHandler extends SQLiteOpenHelper
{
    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MeetUp";

    // Table Names
    private static final String TABLE_USUARIO = "Usuario";
    private static final String TABLE_CATEGORIA_EVENTO = "CategoriaEvento";
    private static final String TABLE_EVENTO = "Evento";
    private static final String TABLE_MENSAGEM = "Mensagem";
    private static final String TABLE_PAGAMENTO = "Pagamento";
    private static final String TABLE_USUARIO_EVENTO = "UsuarioEvento";
    private static final String TABLE_USUARIO_PREFERENCIAS = "UsuarioPreferencias";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_DESC = "desc";
    private static final String KEY_ID_EVENTO = "idEvento";
    private static final String KEY_ID_USUARIO = "idUsuario";
    private static final String KEY_NOME = "nome";
    private static final String KEY_VALOR = "valor";

    // USUARIO Table - column names
    private static final String KEY_DATA_NASCIMENTO = "dataNascimento";
    private static final String KEY_SEXO = "sexo";
    private static final String KEY_FOTO = "foto";
    private static final String KEY_SENHA = "senha";
    private static final String KEY_EMAIL = "email";

    // EVENTO Table - column names
    private static final String KEY_HORARIO_INICIO = "horarioInicio";
    private static final String KEY_HORARIO_FIM = "horarioFim";
    private static final String KEY_LOCAL = "local";
    private static final String KEY_ALCANCE = "alcance";
    private static final String KEY_STATUS = "status";
    private static final String KEY_ID_CATEGORIA_EVENTO = "idCategoriaEvento";

    // MENSAGEM Table - column names
    private static final String KEY_MENSAGEM = "mensagem";
    private static final String KEY_HORARIO = "horario";
    private static final String KEY_USUARIO_EVENTO_EVENT = "usuarioEventoEvent";
    private static final String KEY_USUARIO_EVENTO_USER = "usuarioEventoUser";

    // PAGAMENTO Table - column names
    private static final String KEY_DATA = "data";

    // USUARIOEVENTO Table - column names
    private static final String KEY_ID_CATEGORIA = "idCategoria";

    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_CATEGORIA_EVENTO = "CREATE TABLE "
            + TABLE_CATEGORIA_EVENTO + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DESC
            + " TEXT" + ")";

    // Tag table create statement
    private static final String CREATE_TABLE_EVENTO = "CREATE TABLE " + TABLE_EVENTO
            + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NOME + " TEXT,"
            + KEY_DESC + " TEXT"
            + KEY_HORARIO_INICIO + " DATETIME"
            + KEY_HORARIO_FIM + " DATETIME"
            + KEY_LOCAL + " TEXT"
            + KEY_ALCANCE + " TEXT"
            + KEY_STATUS + " BOOL"
            + KEY_VALOR + " TEXT"
            + KEY_ID_USUARIO + " INTEGER"
            + KEY_ID_CATEGORIA_EVENTO + " INTEGER" + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_MENSAGEM = "CREATE TABLE "
            + TABLE_MENSAGEM + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_MENSAGEM + " TEXT,"
            + KEY_HORARIO + " DATETIME,"
            + KEY_USUARIO_EVENTO_USER + " INTEGER"
            + KEY_USUARIO_EVENTO_EVENT + " INTEGER"
            + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_PAGAMENTO = "CREATE TABLE "
            + TABLE_PAGAMENTO + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_VALOR + " INTEGER,"
            + KEY_ID_EVENTO + " INTEGER"
            + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_USUARIO = "CREATE TABLE "
            + TABLE_USUARIO + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NOME + " TEXT,"
            + KEY_EMAIL + " TEXT"
            + KEY_SENHA + " TEXT"
            + KEY_FOTO + " TEXT"
            + KEY_SEXO + " TEXT"
            + KEY_DATA_NASCIMENTO + " DATETIME"
            + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_USUARIO_EVENTO = "CREATE TABLE "
            + TABLE_USUARIO_EVENTO + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_ID_USUARIO + " INTEGER,"
            + KEY_ID_EVENTO + " INTEGER"
            + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_USUARIO_PREFERENCIAS = "CREATE TABLE "
            + TABLE_USUARIO_PREFERENCIAS + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_ID_USUARIO + " INTEGER,"
            + KEY_ID_CATEGORIA + " INTEGER"
            + ")";

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CATEGORIA_EVENTO);
        db.execSQL(CREATE_TABLE_EVENTO);
        db.execSQL(CREATE_TABLE_MENSAGEM);
        db.execSQL(CREATE_TABLE_PAGAMENTO);
        db.execSQL(CREATE_TABLE_USUARIO);
        db.execSQL(CREATE_TABLE_USUARIO_EVENTO);
        db.execSQL(CREATE_TABLE_USUARIO_PREFERENCIAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
