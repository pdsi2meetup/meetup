package a2pedrocompany.meetup;

import org.apache.http.params.HttpParams;

import java.net.URLConnection;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pedro on 11/23/2016.
 */

public class HttpClient
{
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://meetupufu.tk/api.php/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public ServiceInterface service = retrofit.create(ServiceInterface.class);
}
