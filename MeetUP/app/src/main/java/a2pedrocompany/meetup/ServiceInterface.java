package a2pedrocompany.meetup;

import java.util.Map;

import a2pedrocompany.meetup.Dao.Evento;
import a2pedrocompany.meetup.Dao.Events;
import a2pedrocompany.meetup.Dao.UsuarioEvento;
import a2pedrocompany.meetup.Dao.UsuarioEventos;
import a2pedrocompany.meetup.Dao.Usuarios;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import static android.R.attr.filter;

/**
 * Created by Pedro on 11/23/2016.
 */

public interface ServiceInterface {

        

        @GET("evento/{id}")
        Call<Evento> getEvent(@Path("id") int id);

        @GET("evento")
        Call<Events> getEvents();

        @POST("evento")
        Call<Integer> addEvent(@Body Evento event);

        @PUT("evento/{id}")
        Call<Integer> updateEvent(@Path("id") int id, @Body Evento evento);

        @DELETE("evento/{id}")
        Call<Integer> deleteEvent(@Path("id") int id);

        @GET("usuario")
        Call<Usuarios> doLogin(@Query(value="filter") String filter);

        @GET("usuario_participa_evento")
        Call<UsuarioEventos> getUserEvents();

        @GET("usuario_participa_evento")
        Call<UsuarioEventos> getJoinedUserEvents(@Query(value="filter") String filter);

        @POST("usuario_participa_evento")
        Call<Integer> addParticipaEvento(@Body UsuarioEvento particpa);
}
