package a2pedrocompany.meetup.Dao;

import com.google.gson.annotations.SerializedName;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 12/6/2016.
 */

public class UsuarioEventos
{
    @SerializedName("usuario_participa_evento")
    private UsuarioEvento usuarioEvento;


    public List<UsuarioEvento> ToEventList() throws ParseException
    {
        List<UsuarioEvento> listaUserEventos = new ArrayList<>();


        for (List<String> c : usuarioEvento.getRecords())
        {
            int zero = c.get(0) == null ? 0 : Integer.parseInt(c.get(0));
            int um = c.get(1) == null ? 0 : Integer.parseInt(c.get(1));


            listaUserEventos.add(new UsuarioEvento(zero, um, null, null));
        }

        return listaUserEventos;
    }
}
