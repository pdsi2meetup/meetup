package a2pedrocompany.meetup.Dao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 10/3/2016.
 */

public class UsuarioPreferencias
{
    private int id_usuario;
    private int id_categoria;
    private int id;
    private List<String> columns;
    private ArrayList<List<String>> records;

    public UsuarioPreferencias(int id_usuario, int id_categoria, int id) {
        this.id_usuario = id_usuario;
        this.id_categoria = id_categoria;
        this.id = id;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public ArrayList<List<String>> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<List<String>> records) {
        this.records = records;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
