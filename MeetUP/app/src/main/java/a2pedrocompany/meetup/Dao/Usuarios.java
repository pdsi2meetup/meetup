package a2pedrocompany.meetup.Dao;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Pedro on 12/1/2016.
 */

public class Usuarios
{
    private Usuario usuario;

    DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

    public List<Usuario> toUserList() throws ParseException
    {
        List<Usuario> listaUsuarios = new ArrayList<>();
        Date aTime = null;

        for (List<String> c : usuario.getRecords())
        {
            if(c.get(6) != null) {
                aTime = (Date)format.parse(c.get(6));
            }

            listaUsuarios.add(new Usuario(Integer.parseInt(c.get(0)), c.get(1), c.get(2), c.get(3), c.get(4), c.get(5),
                    aTime, null, null));
        }

        return listaUsuarios;
    }
}
