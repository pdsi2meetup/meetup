package a2pedrocompany.meetup.Dao;

import java.util.Date;

/**
 * Created by Pedro on 10/3/2016.
 */

public class Mensagem
{
    private int id;
    private String mensagem;
    private Date horario;
    private int UsuarioEventoUser;
    private int UsuarioEventoEvent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public int getUsuarioEventoUser() {
        return UsuarioEventoUser;
    }

    public void setUsuarioEventoUser(int usuarioEventoUser) {
        UsuarioEventoUser = usuarioEventoUser;
    }

    public int getUsuarioEventoEvent() {
        return UsuarioEventoEvent;
    }

    public void setUsuarioEventoEvent(int usuarioEventoEvent) {
        UsuarioEventoEvent = usuarioEventoEvent;
    }

    public Mensagem(int id, String mensagem, Date horario, int usuarioEventoUser, int usuarioEventoEvent) {

        this.id = id;
        this.mensagem = mensagem;
        this.horario = horario;
        UsuarioEventoUser = usuarioEventoUser;
        UsuarioEventoEvent = usuarioEventoEvent;
    }
}