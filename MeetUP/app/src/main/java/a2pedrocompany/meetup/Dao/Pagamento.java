package a2pedrocompany.meetup.Dao;

import java.util.Date;

/**
 * Created by Pedro on 10/3/2016.
 */

public class Pagamento
{
    private int id;
    private Double valor;
    private int id_evento;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getId_evento() {
        return id_evento;
    }

    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pagamento(int id, Double valor, int id_evento, Date data) {

        this.id = id;
        this.valor = valor;
        this.id_evento = id_evento;
        this.data = data;
    }

    private Date data;
}
