package a2pedrocompany.meetup.Dao;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 11/24/2016.
 */

public class Events
{
      private Evento evento;

      DateFormat format = new SimpleDateFormat("yyyy/mm/dd hh:mm:ss");

      public List<Evento> ToEventList() throws ParseException {
            List<Evento> listaEventos = new ArrayList<>();

            for (List<String> c : evento.getRecords())
            {
                  int nove = c.get(9) == null ? 0 : Integer.parseInt(c.get(9));
                  int dez = c.get(10) == null ? 0 : Integer.parseInt(c.get(10));
                  float oito = c.get(8) == null ? 0 : Float.parseFloat(c.get(8));
                  double onze = c.get(11) == null ? 0 : Double.parseDouble(c.get(11));
                  double doze = c.get(12) == null ? 0 : Double.parseDouble(c.get(12));

                  listaEventos.add(new Evento(Integer.parseInt(c.get(0)), c.get(1), c.get(2), c.get(3), c.get(4),
                          c.get(5), c.get(6), c.get(7), oito, nove, dez, onze, doze, null, null));
            }

            return listaEventos;
      }
}
