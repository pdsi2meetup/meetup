package a2pedrocompany.meetup.Dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Pedro on 10/3/2016.
 */

public class Usuario
{
    private int id;
    private String nome;
    private String email;
    private String senha;
    private String foto;
    private String sexo;
    private Date data_nascimento;
    private List<String> columns;
    private ArrayList<List<String>> records;

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public ArrayList<List<String>> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<List<String>> records) {
        this.records = records;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public Usuario(int id, String nome, String email, String senha, String foto, String sexo,
                   Date data_nascimento, List<String> listaString, ArrayList<List<String>> resultList)
    {

        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.foto = foto;
        this.sexo = sexo;
        this.data_nascimento = data_nascimento;
        this.columns = listaString;
        this.records = resultList;
    }
}
