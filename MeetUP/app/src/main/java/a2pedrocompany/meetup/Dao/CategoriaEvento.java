package a2pedrocompany.meetup.Dao;

/**
 * Created by Pedro on 10/3/2016.
 */

public class CategoriaEvento
{
    private int id;
    private String desc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public CategoriaEvento(int id, String desc) {

        this.id = id;
        this.desc = desc;
    }
}
