package a2pedrocompany.meetup.Dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 10/3/2016.
 */

public class UsuarioEvento
{
    @SerializedName("evento_id")
    private int id_evento;
    @SerializedName("usuario_id")
    private int id_usuario;
    private List<String> columns;
    private ArrayList<List<String>> records;

    public UsuarioEvento(int id_evento, int id_usuario) {
        this.id_evento = id_evento;
        this.id_usuario = id_usuario;
    }

    public UsuarioEvento() {

    }

    public UsuarioEvento(int zero, int um, Object o, Object o1) {
        id_evento = zero;
        id_usuario = um;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public ArrayList<List<String>> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<List<String>> records) {
        this.records = records;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_evento() {
        return id_evento;
    }

    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

}