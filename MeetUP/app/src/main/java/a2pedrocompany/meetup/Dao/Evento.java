package a2pedrocompany.meetup.Dao;

import android.text.Editable;


import com.google.gson.annotations.SerializedName;

import java.sql.Array;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 10/3/2016.
 */

public class Evento
{

    //region Atributos
    private int id;
    private String nome;
    @SerializedName("descricao")
    private String desc;
    private String horario_inicio;
    private String horario_fim;
    @SerializedName("localizacao")
    private String local;
    private String alcance;
    private String status;
    private float valor;
    @SerializedName("usuario_id")
    private int id_usuario;
    @SerializedName("categoria_evento_id")
    private int id_categoria_evento;
    private double latitude;
    private double longitude;
    private List<String> columns;
    private ArrayList<List<String>> records;
    //endregion

    //region Construtores
    public Evento(int id, String nome, String desc, String horario_inicio, String horario_fim, String local, String alcance, String status,
                  float valor, int id_usuario, int id_categoria_evento, double latitude, double
                          longitude, List<String> columns, ArrayList<List<String>> records) {
        this.id = id;
        this.nome = nome;
        this.desc = desc;
        this.horario_inicio = horario_inicio;
        this.horario_fim = horario_fim;
        this.local = local;
        this.alcance = alcance;
        this.status = status;
        this.valor = valor;
        this.id_usuario = id_usuario;
        this.id_categoria_evento = id_categoria_evento;
        this.columns = columns;
        this.records = records;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Evento(String nome, int i, int i1) {
        this.nome = nome;
        this.id_categoria_evento = i;
        this.id_usuario = i1;
    }

    public Evento()
    {
    }

    public Evento(int id, String name, int i, int j)
    {
        this.id = id;
        this.id_usuario = i;
        this.id_categoria_evento = j;
        this.nome = name;
    }

    //endregion

    //region getsAndSetters

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public ArrayList<List<String>> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<List<String>> records) {
        this.records = records;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHorario_inicio() {
        return horario_inicio;
    }

    public void setHorario_inicio(String horario_inicio) {
        this.horario_inicio = horario_inicio;
    }

    public String getHorario_fim() {
        return horario_fim;
    }

    public void setHorario_fim(String horario_fim) {
        this.horario_fim = horario_fim;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getAlcance() {
        return alcance;
    }

    public void setAlcance(String alcance) {
        this.alcance = alcance;
    }

    public String isStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_categoria_evento() {
        return id_categoria_evento;
    }

    public void setId_categoria_evento(int id_categoria_evento) {
        this.id_categoria_evento = id_categoria_evento;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
//endregion

}