package com.example.pedro.alunos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;

/**
 * Created by Pedro on 10/19/2016.
 */

public class CadastrarAlunoHelper {
    private EditText nome;
    private EditText telefone;
    private EditText endereco;
    private EditText site;
    private EditText email;
    private SeekBar nota;
    private ImageView foto;

    private Aluno aluno;

    public CadastrarAlunoHelper(CadastrarAluno activity) {
        nome = (EditText) activity.findViewById(R.id.nome);
        telefone = (EditText) activity.findViewById(R.id.telefone);
        endereco = (EditText) activity.findViewById(R.id.endereco);
        site = (EditText) activity.findViewById(R.id.site);
        email = (EditText) activity.findViewById(R.id.email);
        nota = (SeekBar) activity.findViewById(R.id.nota);
        foto = (ImageView) activity.findViewById(R.id.foto);
        aluno = new Aluno();
    }

    public Aluno getAluno() {
        aluno.setNome(nome.getText().toString());
        aluno.setTelefone(telefone.getText().toString());
        aluno.setSite(site.getText().toString());
        aluno.setEmail(email.getText().toString());
        aluno.setEndereco(endereco.getText().toString());
        aluno.setNota(Double.valueOf(nota.getProgress()));
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        nome.setText(aluno.getNome());
        telefone.setText(aluno.getTelefone());
        endereco.setText(aluno.getEndereco());
        site.setText(aluno.getSite());
        email.setText(aluno.getEmail());
        nota.setProgress(aluno.getNota().intValue());
        this.aluno = aluno;

        if (aluno.getFoto() != null) {
            carregarFoto(aluno.getFoto());
        }
    }

    public ImageView getFoto() {
        return foto;
    }

    public void carregarFoto (String localFoto) {
        Bitmap imagemFoto = BitmapFactory.decodeFile(localFoto);
        Bitmap imagemReduzida = Bitmap.createScaledBitmap(imagemFoto, 100, 100, true);
        aluno.setFoto(localFoto);
        foto.setImageBitmap(imagemReduzida);
    }
}
