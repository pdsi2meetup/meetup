package com.example.pedro.alunos;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.MenuItemHoverListener;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ListarAlunos extends AppCompatActivity {
    private final String TAG = "CADASTRO_ALUNO";
    private final String ALUNOS_KEY = "LISTA";
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;

    private EditText edNome;
    private Button botao;
    private ListView lvListagem;

    private List<Aluno> listaAlunos;

    private ArrayAdapter<Aluno> adapter;
    private int adapterLayout = android.R.layout.simple_list_item_1;

    private Aluno alunoSelecionado = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_novo:
                Intent intent = new Intent(ListarAlunos.this, CadastrarAluno.class);
                startActivity(intent);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_alunos);

        lvListagem = (ListView) findViewById(R.id.lvListagem);

        registerForContextMenu(lvListagem);

        lvListagem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent form = new Intent(ListarAlunos.this, CadastrarAluno.class);
                alunoSelecionado = (Aluno) lvListagem.getItemAtPosition(i);
                form.putExtra("ALUNO_SELECIONADO", alunoSelecionado);
                startActivity(form);
            }
        });

        lvListagem.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                alunoSelecionado = (Aluno) lvListagem.getItemAtPosition(i);
                Log.i(TAG, "Aluno selecionado Listview.longclick() " + alunoSelecionado.getNome());
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.carregarLista();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        getMenuInflater().inflate(R.menu.menu_contexto, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDeletar:
                excluirAluno();
                break;
            case R.id.menuLigar:
                if (ContextCompat.checkSelfPermission(ListarAlunos.this,  Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ListarAlunos.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                } else {
                    Intent intentLigar = new Intent(Intent.ACTION_CALL);
                    intentLigar.setData(Uri.parse("tel:" + alunoSelecionado.getTelefone()));
                    startActivity(intentLigar);
                }
                break;
            case R.id.menuEnviarSMS:
                Intent intentSMS = new Intent(Intent.ACTION_VIEW);
                intentSMS.setData(Uri.parse("sms:" + alunoSelecionado.getTelefone()));
                intentSMS.putExtra("sms_body","Mensagem de boas vindas :-)");
                startActivity(intentSMS);
                break;
            case R.id.menuAcharNoMapa:
                Intent intentMapa = new Intent(Intent.ACTION_VIEW);
                intentMapa.setData(Uri.parse("geo:0,0?z=14&q=" + alunoSelecionado.getEndereco()));
                startActivity(intentMapa);
                break;
            case R.id.menuNavegarSite:
                Intent intentNavegar = new Intent(Intent.ACTION_VIEW);
                intentNavegar.setData(Uri.parse("http:" + alunoSelecionado.getSite()));
                startActivity(intentNavegar);
                break;
            case R.id.menuEnviarEmail:
                Intent intentEmail = new Intent(Intent.ACTION_SEND);
                intentEmail.setType("message/rfc822");
                intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {alunoSelecionado.getEmail()});
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Falando sobre o curso");
                intentEmail.putExtra(Intent.EXTRA_TEXT, "O curso foi muito legal");
                startActivity(intentEmail);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intentLigar = new Intent(Intent.ACTION_CALL);
                    intentLigar.setData(Uri.parse("tel:" + alunoSelecionado.getTelefone()));
                    startActivity(intentLigar);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void carregarLista() {
        AlunoDAO dao = new AlunoDAO(this);
        this.listaAlunos = dao.listar();
        dao.close();

        this.adapter = new ArrayAdapter<Aluno>(this, adapterLayout, listaAlunos);
        this.lvListagem.setAdapter(adapter);
    }

    private void excluirAluno() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Confirma a exclusão de: " + alunoSelecionado.getNome());
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AlunoDAO dao = new AlunoDAO(ListarAlunos.this);
                dao.deletar(alunoSelecionado);
                dao.close();
                carregarLista();
                alunoSelecionado = null;
            }
        });
        builder.setNegativeButton("Não", null);
        AlertDialog dialog = builder.create();
        dialog.setTitle("Confirmação de operação");
        dialog.show();
    }
}
