package com.example.pedro.alunos;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;

public class CadastrarAluno extends AppCompatActivity {
    private Button botao;
    private CadastrarAlunoHelper helper;

    private Aluno alunoParaSerAlterado = null;

    private String localArquivo;
    private static final int FAZER_FOTO = 123;
    private final int MY_PERMISSIONS_REQUEST_FOTO = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_aluno);

        helper = new CadastrarAlunoHelper(this);
        botao = (Button) findViewById(R.id.botao_cadastrar);

        helper.getFoto().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(CadastrarAluno.this,  Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CadastrarAluno.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_FOTO);
                } else {
                    localArquivo = Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";

                    File arquivo = new File(localArquivo);
                    Uri localFoto = Uri.fromFile(arquivo);
                    Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);
                    startActivityForResult(irParaCamera, FAZER_FOTO);
                }
            }
        });

        alunoParaSerAlterado = (Aluno) getIntent().getSerializableExtra("ALUNO_SELECIONADO");
        if (alunoParaSerAlterado != null) {
            helper.setAluno(alunoParaSerAlterado);
        }

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Aluno aluno = helper.getAluno();
                AlunoDAO dao = new AlunoDAO(CadastrarAluno.this);
                if (aluno.getId() == null) {
                    dao.cadastrar(aluno);
                } else {
                    dao.alterar(aluno);
                }

                dao.close();
                //Toast.makeText(CadastrarAluno.this, "Aluno: " + aluno.getNome(), Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FOTO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    localArquivo = Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";

                    File arquivo = new File(localArquivo);
                    Uri localFoto = Uri.fromFile(arquivo);
                    Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);
                    startActivityForResult(irParaCamera, FAZER_FOTO);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FAZER_FOTO) {
            if(resultCode == Activity.RESULT_OK) {
                helper.carregarFoto(this.localArquivo);
            } else {
                localArquivo = null;
            }
        }
    }
}
